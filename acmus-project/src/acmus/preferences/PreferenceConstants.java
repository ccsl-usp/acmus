package acmus.preferences;

/**
 * Constant definitions for plug-in preferences
 */
public class PreferenceConstants {

	public static final String P_AUDIO_OUTPUT = "audioOutput";

	public static final String P_AUDIO_INPUT = "audioInput";

}
